const mongoose = require('mongoose')
const bcryptNodejs = require('bcrypt-nodejs');
const crypto = require('crypto');

const schema = mongoose.Schema;

const userSC =  new schema({
    email: {type: String , unique: true, lowercase: true},
    displatName: String,
    avatar: String,
    pass: {type: String, select: false},
    signUpDate: {type:Date, default: Date.now()},
    lasLoggin: Date
});

userSC.pre('save', (next)=>{
    let user = this;
    if(!user.isModified('pass'))return next();
    bcryptNodejs.genSalt(10, (err, salt)=>{
        if (err) return next(err);
        bcryptNodejs.hash(user.pass, salt, null, (err, hash) =>{
            if (err) return next(err);
            user.pass = hash;
            next();
        });
    });

});

userSC.methods.gravatar =  () =>{
    if (!this.email) return `https://gravatar.com/avatar?s=200&d=retro`;

    const md5 = crypto.createHash('md5').update(this.email).digest('hex');

    return `https://gravatar.com/avatar/${md5}?s=200&d=retro`;
};

module.exports = mongoose.model('User', userSC);

