const mongoose = require('mongoose')
const schema = mongoose.Schema;

const pdSchema = schema({
    name: String,
    picture: String,
    price: {type: Number, default: 0},
    category: {type: String, enum: ['laptops', 'phones', 'aceesories']},
    desc: String
});

module.exports = mongoose.model('Product', pdSchema);