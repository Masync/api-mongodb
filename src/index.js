
const mongoose = require('mongoose');
require('dotenv').config({ path: 'src/.env' });
const app = require('../src/app');

/* init*/


/** Start and connect to mongo */
mongoose.connect(process.env.CONNECTDB,{useNewUrlParser: true, useUnifiedTopology: true}, ()=>{ 
        app.listen(app.get('port'), () =>{
            console.log(`port ${app.get('port')}`);   
        });
    });
