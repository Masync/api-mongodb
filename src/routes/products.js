const express = require('express');
const controller = require('../controllers/products');
const route = express.Router();

/** Http Verbs */
route.get('/products', controller.getProducts);

route.get('/products/:id', controller.getProductID);

route.post('/products', controller.postProduct);

route.put('/products/:id', controller.UpdateProduct);

route.delete('/products/:id', controller.deleteProduct);

module.exports = route;