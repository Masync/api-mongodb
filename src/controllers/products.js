const Product = require('../models/products');

function getProductID(req, res) {
    let productId = req.params.id;
    Product.findById(productId, (err, data) =>{
        if(err) res.status(500).send({message: `error con la peticion`});
        if(!data) res.status(404).send({message: `no se encontro elemento`});
        res.status(200).send({data});
    });
};

function getProducts(req, res) {
    Product.find({}, (err, info)=>{
        if(err) res.status(500).send({message: `error con la peticion`});
        if(!info) res.status(404).send({message: `no se encontro elemento`});
        res.status(200).send({info});
    });
};

function postProduct(req, res) {
    let product = new Product();
    product.name = req.body.name;
    product.picture = req.body.picture;
    product.price = req.body.price;
    product.category = req.body.category;
    product.desc = req.body.desc;
    
    product.save((err, pStorage)=>{
        if (err) res.status(500).send({message:`error al guardar`});
        
        res.status(200).send({product: pStorage});
    }); 
};

function deleteProduct(req, res) {
    let productId = req.params.id;
    Product.findById(productId, (err, data) =>{
        if(err) res.status(500).send({message: `error con la peticion`});
        if(!data) res.status(404).send({message: `no se encontro elemento`});
        data.remove(e =>{
            if(err) res.status(500).send({message: `error con la peticion`});
            res.status(200).send({message: 'product deleted'});
        });

    });
};

function UpdateProduct(req, res) {
    let productId = req.params.id;
    let update = req.body;
    Product.findByIdAndUpdate(productId,update, (err, data) =>{
        if(err) res.status(500).send({message: `error con la peticion`});
        if(!data) res.status(404).send({message: `no se encontro elemento`});
        res.status(200).send({data});

    });
};


module.exports = {
    getProductID,
getProducts,
postProduct,
deleteProduct,
UpdateProduct
};