/* Libs*/
const bodyParser = require('body-parser');
const express = require('express');
const api = require('../src/routes/products');
require('dotenv').config({ path: 'src/.env' });

const app = express();

/** Settings */
app.set('port', process.env.PORT);

/** Middleware */
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use('/api', api);



module.exports = app;